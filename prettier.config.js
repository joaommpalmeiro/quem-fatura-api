// Source: https://github.com/feedzai/prettier-config

module.exports = {
    arrowParens: "always",
    bracketSameLine: false,
    bracketSpacing: true,
    printWidth: 120,
    proseWrap: "preserve",
    // quoteProps: "preserve",
    quoteProps: "as-needed",
    semi: true,
    singleQuote: false,
    tabWidth: 4,
    trailingComma: "all",
    useTabs: false,
    overrides: [
        {
            files: ["*.md", "*.yml", "*.yaml"],
            options: {
                tabWidth: 2,
            },
        },
    ],
};

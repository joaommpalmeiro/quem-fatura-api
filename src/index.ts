import app from "./server";

const defaultPort = 3000;
const port = process.env.PORT || defaultPort;

app.listen(port, () => {
    // eslint-disable-next-line no-console
    console.log(`server on http://localhost:${port}`);
});

import { Request, Response } from "express";

import prisma from "../db";

// https://www.dawsoncodes.com/posts/5/building-a-rest-api-with-prisma-and-expressjs
// https://typescript-eslint.io/rules/no-misused-promises/
export const getCompany = async (req: Request, res: Response) => {
    const nipc = req.params.number;
    // console.log(typeof n);

    const company = await prisma.company.findUnique({
        where: {
            nipc: parseInt(nipc, 10),
        },
        include: {
            brands: true,
        },
    });

    res.json({ data: company });
};

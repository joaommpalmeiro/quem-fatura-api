import express, { Request, Response, urlencoded } from "express";
import helmet from "helmet";
import morgan from "morgan";

import router from "./router";

// API
const app = express();

app.use(morgan("dev"));

// https://helmetjs.github.io/
app.use(helmet());

// https://expressjs.com/en/api.html#express.urlencoded
// https://www.npmjs.com/package/qs
app.use(urlencoded({ extended: false }));

app.get("/", (req: Request, res: Response) => {
    // console.log("Hello, World!");
    // res.send("Hello, World!");
    res.json({ message: "Hello, World!" });
});

app.use("/api", router);

export default app;

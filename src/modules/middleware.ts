import { RequestHandler } from "express";
import { validationResult } from "express-validator";

// https://stackoverflow.com/a/49021034
// https://express-validator.github.io/docs/index.html
// https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
export const handleInputErrors: RequestHandler = (req, res, next) => {
    const errors = validationResult(req);
    // console.log(errors);

    if (!errors.isEmpty()) {
        const badRequestCode = 400;
        res.status(badRequestCode).json({ errors: errors.array() });
    } else {
        next();
    }
};

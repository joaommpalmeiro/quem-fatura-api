import { Router } from "express";
import { param } from "express-validator";
import isTaxID from "validator/lib/isTaxID";

import { getCompany } from "./handlers/company";
import { handleInputErrors } from "./modules/middleware";

const COMPANY_PREFIX = "5";

const router = Router();

// https://github.com/validatorjs/validator.js/blob/13.7.0/src/lib/isVAT.js
// https://github.com/validatorjs/validator.js/blob/master/src/lib/isVAT.js
// https://github.com/validatorjs/validator.js/blob/13.7.0/src/lib/util/algorithms.js#L47
// https://express-validator.github.io/docs/custom-validators-sanitizers.html
// https://github.com/asmarques/pt-id
// https://github.com/asmarques/pt-id/blob/master/lib/nif.js
// https://github.com/asmarques/pt-id/blob/master/lib/id.js
// https://github.com/LiosK/cdigit
// https://regex101.com/
// https://europa.eu/youreurope/business/taxation/vat/check-vat-number-vies/index_pt.htm
// https://en.wikipedia.org/wiki/VAT_identification_number
// https://pt.wikipedia.org/wiki/N%C3%BAmero_de_identifica%C3%A7%C3%A3o_fiscal
// https://gist.github.com/dreispt/024dd11c160af58268e2b44019080bbf
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test
// https://express-validator.github.io/docs/custom-error-messages.html
// https://express-validator.github.io/docs/index.html
// https://github.com/validatorjs/validator.js/blob/13.7.0/src/lib/isTaxID.js#L937
router.get(
    "/nipc/:number",
    param("number").custom((value) => {
        // console.log(typeof value, value);

        const isNumber = /^\d{9}$/.test(value as string);
        if (!isNumber) {
            // return false;
            throw new Error("The value is not a 9-digit number.");
        }

        const isNIF = isTaxID(value as string, "pt-PT");
        if (!isNIF) {
            // return false;
            throw new Error("The value is not a valid VAT identification number (NIF).");
        }

        // eslint-disable-next-line no-magic-numbers
        const isNIPC = (value as string).substring(0, 1) === COMPANY_PREFIX;
        if (!isNIPC) {
            // return false;
            throw new Error("The value is not a NIPC.");
        }

        return true;
    }),
    handleInputErrors,
    getCompany,
);

export default router;

/* eslint-disable no-console */

// https://github.com/prisma/prisma-examples/blob/latest/typescript/rest-express/prisma/seed.ts
// https://www.prisma.io/docs/guides/database/seed-database#how-to-seed-your-database-in-prisma
// https://www.prisma.io/docs/concepts/components/prisma-client/crud#update-or-create-records
// https://www.prisma.io/docs/concepts/components/prisma-client/crud#create-a-single-record
// https://github.com/Darth-Knoppix/next-prisma-seed-example/blob/main/prisma/seed.ts
// https://eslint.org/docs/latest/rules/no-await-in-loop
// https://www.prisma.io/docs/concepts/components/prisma-client/relation-queries#nested-writes
// https://www.prisma.io/docs/concepts/components/prisma-client/relation-queries#connect-or-create-a-record
// https://github.com/prisma/prisma/discussions/4382
// https://www.prisma.io/docs/concepts/components/prisma-client/null-and-undefined
// https://bobbyhadz.com/blog/javascript-check-if-string-is-empty
// npx ts-node prisma/seed.ts

import { PrismaClient } from "@prisma/client";

import data from "./data.json";

// console.log(data);
// const EMPTY_LENGTH = 0;

const prisma = new PrismaClient();

function main() {
    console.log("Start seeding…");

    Promise.all(
        data.map((datum) =>
            prisma.brand.create({
                data: {
                    name: datum.brand,
                    aliases: datum.aliases,
                    website: datum.website,
                    // source: datum.source.trim().length === EMPTY_LENGTH ? undefined : datum.source,
                    source: datum.source,
                    createdAt: datum.last_update,

                    company: {
                        connectOrCreate: {
                            where: {
                                nipc: datum.nipc,
                            },
                            create: {
                                name: datum.company,
                                nipc: datum.nipc,
                                createdAt: datum.last_update,
                            },
                        },
                    },
                },
            }),
        ),
    )
        .then(async () => {
            console.log("Seeding finished.");
            await prisma.$disconnect();
        })
        .catch(async (e) => {
            console.error(e);
            await prisma.$disconnect();
            // eslint-disable-next-line no-magic-numbers
            process.exit(1);
        });
}

main();

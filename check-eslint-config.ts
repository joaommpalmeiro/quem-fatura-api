/* eslint-disable no-console */
/* eslint-disable import/no-extraneous-dependencies */

// https://www.stefanjudis.com/today-i-learned/top-level-await-is-available-in-node-js-modules/
// https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/eslint

import { ESLint } from "eslint";

// const REF_FILE = "./.eslintrc.js";
const REF_FILE = "./src/index.ts";

const eslint = new ESLint({ useEslintrc: true });

eslint
    .calculateConfigForFile(REF_FILE)
    .then((config) => {
        // npx ts-node check-eslint-config.ts > formatter.json
        console.log(JSON.stringify(config));
        // npx ts-node check-eslint-config.ts
        // console.log(config);
    })
    .catch((error) => {
        process.exitCode = 1;
        console.error(error);
    });

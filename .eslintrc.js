// const ignorePatternForComments = ["const", "console", "npx"].join("|");

module.exports = {
    root: true,
    env: {
        node: true,
        browser: false,
    },
    extends: [
        "eslint:recommended",
        // "plugin:@typescript-eslint/eslint-recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:@typescript-eslint/recommended-requiring-type-checking",
        "plugin:import/recommended",
        "plugin:import/typescript",
        "plugin:security/recommended",
        "plugin:prettier/recommended",
    ],
    parser: "@typescript-eslint/parser",
    parserOptions: {
        tsconfigRootDir: __dirname,
        project: ["./tsconfig.eslint.json"],
    },
    plugins: ["@typescript-eslint"],
    // https://eslint.org/docs/latest/rules/
    // https://typescript-eslint.io/rules/
    // https://github.com/typescript-eslint/typescript-eslint/blob/v5.46.0/packages/eslint-plugin/src/configs/eslint-recommended.ts
    // https://github.com/prettier/eslint-config-prettier/blob/v8.5.0/index.js
    rules: {
        "no-console": "warn",
        "no-else-return": "error",
        eqeqeq: "error",
        "no-self-compare": "error",
        "no-template-curly-in-string": "error",
        // https://eslint.org/docs/latest/rules/no-await-in-loop
        "no-await-in-loop": "error",
        // https://eslint.org/docs/latest/rules/no-constant-binary-expression
        "no-constant-binary-expression": "error",
        // https://eslint.org/docs/latest/rules/no-constructor-return
        "no-constructor-return": "error",
        // https://eslint.org/docs/latest/rules/no-new-native-nonconstructor
        "no-new-native-nonconstructor": "error",
        // https://eslint.org/docs/latest/rules/no-unmodified-loop-condition
        "no-unmodified-loop-condition": "error",
        // https://eslint.org/docs/latest/rules/no-unreachable-loop
        "no-unreachable-loop": "error",
        // https://eslint.org/docs/latest/rules/no-unused-private-class-members
        "no-unused-private-class-members": "error",
        // https://eslint.org/docs/latest/rules/no-use-before-define
        "no-use-before-define": "error",
        // https://eslint.org/docs/latest/rules/capitalized-comments
        // "capitalized-comments": [
        //     "error",
        //     "always",
        //     { ignorePattern: ignorePatternForComments, ignoreInlineComments: false, ignoreConsecutiveComments: true },
        // ],
        // https://eslint.org/docs/latest/rules/no-alert
        "no-alert": "error",
        // https://eslint.org/docs/latest/rules/symbol-description
        "symbol-description": "error",
        // https://eslint.org/docs/latest/rules/sort-vars
        "sort-vars": ["error", { ignoreCase: true }],
        // https://eslint.org/docs/latest/rules/sort-keys
        // "sort-keys": ["error", "asc", { caseSensitive: false, natural: true, minKeys: 2 }],
        // https://eslint.org/docs/latest/rules/radix
        radix: "error",
        // https://eslint.org/docs/latest/rules/no-warning-comments
        "no-warning-comments": "warn",
        // https://eslint.org/docs/latest/rules/no-script-url
        "no-script-url": "error",
        // https://eslint.org/docs/latest/rules/no-magic-numbers
        "no-magic-numbers": "error",
        // https://eslint.org/docs/latest/rules/sort-imports
        // https://github.com/import-js/eslint-plugin-import/issues/1732
        // https://github.com/import-js/eslint-plugin-import/issues/2085
        "sort-imports": [
            "error",
            {
                ignoreDeclarationSort: true,
            },
        ],
        "import/no-extraneous-dependencies": [
            "error",
            { devDependencies: false, optionalDependencies: false, peerDependencies: false },
        ],
        "import/order": [
            "error",
            {
                "newlines-between": "always",
                alphabetize: {
                    order: "asc",
                    caseInsensitive: true,
                },
            },
        ],
        "@typescript-eslint/naming-convention": [
            "error",
            { selector: "variableLike", format: ["camelCase"] },
            {
                selector: "variable",
                format: ["camelCase", "UPPER_CASE"],
            },
            {
                selector: "variable",
                types: ["boolean"],
                format: ["PascalCase"],
                prefix: ["is"],
            },
        ],
    },
    settings: {
        "import/resolver": {
            typescript: {
                alwaysTryTypes: true,
                project: "./tsconfig.json",
            },
        },
    },
};
